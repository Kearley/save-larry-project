### IMPORTS ###

import sqlite3

### GLOBALS ####
dbname = 'inventory.sqlite'

### FUNCTIONS ####

def main():
    ################################################
    # Open/create the SQL DB with a connection cursor. 
    ###

    conn = sqlite3.connect(dbname)
    cur = conn.cursor()


    ################################################
    # Create a "fresh" DEVICES table in the DB. 
    ###

    cur.executescript('''
    DROP TABLE IF EXISTS DEVICES;


    CREATE TABLE DEVICES (
        hostname  TEXT NOT NULL PRIMARY KEY UNIQUE,
        serialnumber  TEXT, 
        software  TEXT, 
        model  TEXT 
    );

    ''')

if __name__ == "__main__":
    main()

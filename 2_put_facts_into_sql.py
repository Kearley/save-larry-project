### IMPORTS ###

import time
from jnpr.junos import Device
from jnpr.junos.exception import *
from pprint import pprint as pp
import argparse, getpass, csv
import sqlite3

### GLOBALS ####
device_info = ['hostname', 'serialnumber', 'software', 'model']
dbname = 'inventory.sqlite'


### FUNCTIONS ####

def get_args():
    ''' Collect runtime arguments from the user using argparse() and return them 
    for later reuse. '''
    parser = argparse.ArgumentParser(description="A script to get facts from \
        Junos devices.")
    parser.add_argument('-d', '--device', nargs='+', help='Device hostname|ip \
        or a list of hostnames|ips, separated by a space.')
    parser.add_argument('-f', '--file', help='Filename to store device \
        output information. Defaults to inventory.csv if none given.', \
        default='inventory.csv')
    parser.add_argument('-u', '--user', help='The admin username to login into\
        the device(s). Defaults to the current user if none is provided.', \
        default=getpass.getuser())

    args = parser.parse_args()

    return(args)

def get_facts(host, user, passwd):
    ''' Login to the device and collect specific device facts using Junos Py-EZ 
        get_facts() and return them as a dictionary. If there is a login error, 
        report that to the user.'''
    try:
        with Device(host=host,user=user,passwd=passwd) as dev:
            device = {}
            device['hostname'] = dev.facts['hostname']
            device['serialnumber'] = dev.facts['serialnumber']
            device['software'] = dev.facts['version']
            device['model'] = dev.facts['model']

            ''' Debug - print collected facts as a single string '''
            # print("hostname: %s, serialnumber: %s, software: %s, model: %s " % \
            #     (dev.facts['hostname'], dev.facts['serialnumber'], \
            #     dev.facts['version'], dev.facts['model']))

        return(device)
    except ConnectRefusedError:
        print(host + ' - Error: Device connection refused!')
    except ConnectTimeoutError:
        print(host + ' - Error: Device connection timed out!')
    except ConnectAuthError:
        print(host + ' - Error: Authentication failure!')


def write_to_sql(dbname, devices):
    ''' Write the devices list to SQL.  Assumes SQL DB exists with table == DEVICES. '''

    ''' Create a connection and cursor handle to the SQL DB '''
    conn = sqlite3.connect(dbname)
    cur = conn.cursor()

    ''' Iterate over the devices list '''
    for i in devices:
        ''' Debug - print devices list or dictionaries '''
        # pp(i)

        ''' Set local variables to key values in devices '''
        hostname = i['hostname']
        serialnumber = i['serialnumber']
        model = i['model']
        software = i['software']
        
        ''' Write to SQL table DEVICES  '''
        cur.execute('''INSERT OR IGNORE INTO DEVICES (hostname, serialnumber, software, model) \
        VALUES ( ?, ?, ?, ? )''', ( hostname, serialnumber, software, model,))
        conn.commit()


def main():
    ''' Parse the the user's device runtime options and ask for their password. '''
    args = get_args()
    passwd = getpass.getpass()

    ''' Debug - get the start time if you are reporting script execution time. '''
    st = time.time()

    ''' Create an empty list for storing device returns as collected. '''
    devices = []

    ''' Iterate over each device, collect required information, and append to the 
    list.  But only store devices when there is information returned. '''
    for host in args.device:
        device = get_facts(host, args.user, passwd)
        # pp(device)
        if device:
            devices.append(device)

    ''' Debug - show the devices list '''
    # print('\n')
    # pp(devices)

    ''' Write to SQL '''
    write_to_sql(dbname, devices)

    ''' Debug - get the end time and calculate total runtime '''
    et = time.time()
    elapsed_time = et - st
    print('\nExecution time: {0:2f} seconds'.format (elapsed_time))

if __name__ == "__main__":
    main()

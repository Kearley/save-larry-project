### IMPORTS ###

import time
from jnpr.junos import Device
from jnpr.junos.exception import *
from pprint import pprint as pp
import argparse, getpass, csv

### GLOBALS ####
device_info = ['hostname', 'serialnumber', 'software', 'model']

### FUNCTIONS ####

def get_args():
    ''' Collect runtime arguments from the user using argparse() and return them 
    for later reuse. '''
    parser = argparse.ArgumentParser(description="A script to get facts from \
        Junos devices.")
    parser.add_argument('-d', '--device', nargs='+', help='Device hostname|ip \
        or a list of hostnames|ips, separated by a space.')
    parser.add_argument('-f', '--file', help='Filename to store device \
        output information. Defaults to inventory.csv if none given.', \
        default='inventory.csv')
    parser.add_argument('-u', '--user', help='The admin username to login into\
        the device(s). Defaults to the current user if none is provided.', \
        default=getpass.getuser())

    args = parser.parse_args()

    return(args)

def get_facts(host, user, passwd):
    ''' Login to the device and collect specific device facts using Junos Py-EZ 
        get_facts() and return them as a dictionary. If there is a login error, 
        report that to the user.'''
    try:
        with Device(host=host,user=user,passwd=passwd) as dev:
            device = {}
            device['hostname'] = dev.facts['hostname']
            device['serialnumber'] = dev.facts['serialnumber']
            device['software'] = dev.facts['version']
            device['model'] = dev.facts['model']

            ''' Debug - print collected facts as a single string '''
            # print("hostname: %s, serialnumber: %s, software: %s, model: %s " % \
            #     (dev.facts['hostname'], dev.facts['serialnumber'], \
            #     dev.facts['version'], dev.facts['model']))

        return(device)
    except ConnectRefusedError:
        print(host + ' - Error: Device connection refused!')
    except ConnectTimeoutError:
        print(host + ' - Error: Device connection timed out!')
    except ConnectAuthError:
        print(host + ' - Error: Authentication failure!')

def write_to_file(file_name, devices):
    with open(file_name, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames = device_info)
        writer.writeheader()
        writer.writerows(devices)

def main():
    ''' Parse the the user's device runtime options and ask for their password. '''
    args = get_args()
    passwd = getpass.getpass()

    ''' Debug - get the start time if you are reporting script execution time. '''
    # st = time.time()

    ''' Create an empty list for storing device returns as collected. '''
    devices = []

    ''' Iterate over each device, collect required information, and append to the 
    list.  But only store devices when there is information returned. '''
    for host in args.device:
        device = get_facts(host, args.user, passwd)
        # pp(device)
        if device:
            devices.append(device)

    ''' Debug - show the devices list '''
    # print('\n')
    # pp(devices)

    ''' Write to .csv file '''
    write_to_file(args.file, devices)

    ''' Debug - get the end time and calculate total runtime '''
    # et = time.time()
    # elapsed_time = et - st
    # print('\nExecution time: {0:2f} seconds'.format (elapsed_time))

if __name__ == "__main__":
    main()

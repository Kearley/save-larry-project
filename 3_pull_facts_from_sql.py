### IMPORTS ###

import time
import argparse, csv
import sqlite3

### GLOBALS ####
device_info = ['hostname', 'serialnumber', 'software', 'model']
my_dbname = 'inventory.sqlite'


### FUNCTIONS ####

def get_args():
    ''' Collect runtime arguments from the user using argparse() and return them 
    for later reuse. '''
    parser = argparse.ArgumentParser(description="A script to get facts from \
        SQL-Lite device database.")
    parser.add_argument('-d', '--database', help='Database to query. Defaults \
        to '+ my_dbname + ' if none given.', default=my_dbname)
    parser.add_argument('-f', '--file', help='Filename to store device \
        output information. Defaults to inventory.csv if none given.', \
        default='inventory.csv')

    args = parser.parse_args()

    return(args)

def sql_to_file(dbname, filename):
    conn = sqlite3.connect(dbname)
    cur = conn.cursor()
    cur.execute("SELECT * FROM DEVICES")
    # rows = cur.fetchall()

    try :
        f1 = open(filename, 'w')
        with open(filename, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow([i[0] for i in cur.description])
            csv_writer.writerows(cur)
    except :
        print('This file cannot be opened: ', filename)
        quit()


def main():
    ''' Parse the the user's device runtime options and ask for their password. '''
    args = get_args()

    ''' Debug - get the start time if you are reporting script execution time. '''
    st = time.time()

    ''' Query the SQL DB for all contents and write to .csv file '''
    sql_to_file(args.database, args.file)

    ''' Debug - get the end time and calculate total runtime '''
    et = time.time()
    elapsed_time = et - st
    print('\nExecution time: {0:2f} seconds'.format (elapsed_time))

if __name__ == "__main__":
    main()
